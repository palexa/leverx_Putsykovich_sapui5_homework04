OrderInfo = {
    id			: "",
    createdAt	: "",
    customer	: "",
    status		: "",
    shippedAt	: ""
};
var Product = {
    name		: "",
    id			: "",
    price		: "",
    currency	: "",
    quantity	: "",
    totalPrice	: ""
};
var ShipTo = {
    name	: "",
    address	: "",
    ZIP		: "",
    region	: "",
    country	: ""
};
CustomerInfo = {
    firstName	: "",
    lastName	: "",
    address		: "",
    phone		: "",
    email		: "",
    photo		: ""
};
Order = {
    id			: "",
    OrderInfo	: {},
    ShipTo		: {},
    CustomerInfo: {},
    products	: []
};
var Orders;
var arr;
var shippingButtonOn=true;



/**
 * Model class. Knows everything about API endpoint and data structure. Can format/map data to any structure.
 *
 * @constructor
 */
var _currentOrder = 0;
function Model() {

    var apiPrefix = "http://localhost:3000/api/";
    this. getURL=function(url,callback){
        var xhr =new XMLHttpRequest();
        xhr.open("GET",url,true);
        xhr.onreadystatechange=function () {
            if(this.status<400&&this.responseText!=false) {
                Orders=JSON.parse(this.responseText);
                callback(Orders);
            }else
            console.error("Request fail:"+this.statusText);
        };

        xhr.send(null);
    }


    this.sendRequest = function (method, url, data) {
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            req.open(method, apiPrefix + url, true);

            req.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

            req.addEventListener("load", function () {
                if (req.status < 400) {
                    resolve(req.responseText);
                } else {
                    reject(new Error("Request failed: " + req.statusText));
                }
            });

            req.addEventListener("error", function () {
                reject(new Error("Network error"));
            });
            console.log(data);
            console.log("json: "+JSON.stringify(data));
            req.send(JSON.stringify(data));

        });
    };


    this.sendRequestOnDelete = function (url) {

        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            req.open("DELETE", apiPrefix + url, true);

            req.addEventListener("load", function () {
                if (req.status < 400) {
                    resolve(req.status);
                } else {
                    reject(new Error("Request failed: " + req.statusText));
                }
            });

            req.addEventListener("error", function () {
                reject(new Error("Network error"));
            });

            req.send();

        })
    };

    this.createOrder = function (order) {
        var that = this;
        return this.sendRequest("POST", "orders", order)
            .then(function () {
                //return viewContext.viewAllOrders();
            })
            .then(function (data) {
                var orders = data;
                return orders;
            })
            .catch(function (error) {
                console.log(error.name + error)
            })
    };

    this.createProduct = function (product) {
        var that = this;
        return this.sendRequest("POST", "OrderProducts", product)
            .then(function () {
                return that.refreshProductsTable(product.orderId);
            })
            .catch(function (error) {
                console.log(error.name + error);
            })
    };


    this.refreshListOrders = function () {
        return this.sendRequest("GET", "orders")
            .then(function (data) {
                var orders = data;
                return orders;
            })
            .catch(function (error) {
                console.log(error.name + error);
            });
    };

    this.refreshProductsTable = function (orderId) {
        return this.sendRequest("GET", "orders/" + orderId + "/products")
            .then(function (data) {
                var products = data;
                return products;
            })
            .catch(function (error) {
                console.log(error.name + error);
            });
    };

    this.fetchOrderById = function (orderId) {
        return this
            .sendRequest("GET", "orders/" + orderId)
            .then(function (order) {
                return order;
            })
            .catch(function (error) {
                console.log(error.name + error)
            })
    };

    this.getCurrentOrder=function (listForm,searchInput) {
        return _currentOrder;
    }

    this.Searching=function(){
        var ul = document.getElementById("ul");
        var searchInput=document.getElementById("searchInput");

        searchInput.addEventListener("change", function(event) {
            var value = event.target.value.toLowerCase();
            ul.innerHTML="";
            for(i=0;i<Orders.length; i++){
                var obj =  Orders[i]["summary"];
                var propertyArray=Object.values(obj);
                for(j=0; j<propertyArray.length; j++){
                    var searchContainer= propertyArray[j].toLowerCase();
                    var orderSearchId = Orders[i]["id"];
                    var res = search(value, searchContainer);
                    if(res === 1){
                        createList(orderSearchId-1);
                    }
                }

            }

        });
    }

    this.find=function (value) {
        for(i=0;i<Orders.length; i++){
            var obj =  Orders[i]["summary"];
            var propertyArray=Object.values(obj);
            for(j=0; j<propertyArray.length; j++){
                var searchContainer= propertyArray[j].toLowerCase();
                var orderSearchId = Orders[i]["id"];
                var res = this.search(value, searchContainer);
                if(res === 1){
                    return i;
                }
            }

        }
    }

    this.search=function(re, str) {
        if (str.search(re) != -1) {
            return 1;
        } else {
            return 0;
        }
    }

    this.fetchData = function (url) {
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();

            req.open("GET", url, true);

            // listen to load event
            req.addEventListener("load", function () {
                if (req.status < 400) {
                    resolve(JSON.parse(req.responseText).d);
                } else {
                    reject(new Error("Request failed: " + req.statusText));
                }
            });

            // listen to error event
            req.addEventListener("error", function () {
                reject(new Error("Network error"));
            });

            req.send();
        });
    }/////LIKE EXAMPLE
    
    

    

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function View() {

    var viewContext = this;

    this.getSearchInputField = function () {
        return document.querySelector("#SearchInput");
    };
    this.getSearchInputButton = function () {
        return document.querySelector("#Submit");
    };
    this.getListForm = function () {
        return document.querySelector("#ul");
    };
    this.getShippingAddressButton = function () {
        return document.querySelector("#Shipping_Address_Button");
    };
    this.getCustomerInfoButton = function () {
        return document.querySelector("#Customer_Info_Button");
    };
    this.getCurrentSearchValue=function () {
        return this.getSearchInputField().value.toLowerCase();
    }
    this.getCloseOrderModelButton = function () {
        return document.querySelector(".closeOrder");
    };
    this.getCloseProductModelButton = function () {
        return document.querySelector(".closeProduct");
    };
    this.getCloseUpdateModelButton = function () {
        return document.querySelector(".closeUpdate");
    };
    this.getUpdateInfoButton = function () {
        return document.querySelector("#updateAddress");
    };
    this.getAddProductButton = function () {
        return document.querySelector("#addProduct");
    };
    this.getSubOrderButton = function () {
        return document.querySelector("#subOrder");
    };
    this.getSummaryCustomerValue = function () {
        return document.querySelector(".summaryCustomer").value;
    };
    this.getSummaryShippedAtValue = function () {
        return document.querySelector(".summaryShippedAt").value;
    };
    this.getSummaryStatusValue = function () {
        return document.querySelector(".summaryStatus").value;
    };
    this.getSummaryTotalPriceValue = function () {
        return document.querySelector(".summaryTotalPrice").value;
    };

    this.getShipToNameValue = function () {
        return document.querySelector(".shipToName").value;
    };
    this.getShipToStreetValue = function () {
        return document.querySelector(".shipToStreet").value;
    };
    this.getShipToZipCodeValue = function () {
        return document.querySelector(".shipToZipCode").value;
    };
    this.getShipToRegionValue = function () {
        return document.querySelector(".shipToRegion").value;
    };
    this.getShipToCountryValue = function () {
        return document.querySelector(".shipToCountry").value;
    };

    this.getCustomerInfoFirstNameValue = function () {
        return document.querySelector(".customerInfoFirstName").value;
    };
    this.getCustomerInfoLastNameValue = function () {
        return document.querySelector(".customerInfoLastName").value;
    };
    this.getCustomerInfoAddressValue = function () {
        return document.querySelector(".customerInfoAddress").value;
    };
    this.getCustomerInfoPhoneValue = function () {
        return document.querySelector(".customerInfoPhone").value;
    };
    this.getCustomerInfoEmailValue = function () {
        return document.querySelector(".customerInfoEmail").value;
    };

    this.getUpdateNameValue = function () {
        return document.querySelector("#updateName").value;
    };
    this.getUpdateStreetValue = function () {
        return document.querySelector("#updateStreet").value;
    };
    this.getUpdateZipCodeValue = function () {
        return document.querySelector("#updateZipCode").value;
    };
    this.getUpdateRegionValue = function () {
        return document.querySelector("#updateRegion").value;
    };
    this.getUpdateCountryValue = function () {
        return document.querySelector("#updateCountry").value;
    };
    this.getProductModelInputs = function () {
        return document.querySelectorAll("#productModel input");
    };
    this.getMapButton = function () {
        return document.querySelector("#mapButton");
    };
    this.getSubInfoButton = function () {
        return document.querySelector("#subInfo");
    };

    this.openModel = function (model) {
        document.querySelector("#" + model).style.display = "block";
        return this;
    };

    this.getOrderModelInputs = function () {
        return document.querySelectorAll("#orderModel input");
    };

    this.getUpdateModelInputs = function () {
        return document.querySelectorAll("#updateModel input");
    };

    this.clearInputs = function (inputs) {
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].value = "";
        }
        return this;
    };

    this.setCurrentValue = function (inputs, currentOrder) {
        var i = 0;
        for (var key in currentOrder.shipTo) {
            inputs[i].value = currentOrder.shipTo[key];
            i++;
        }
    };

    this.closeModel = function (model) {
        document.querySelector("#" + model).style.display = "none";
        return this;
    };
    this.getInfo=function () {
        var info=document.querySelector("#Info");
        return info;

    }
    this.getShip=function () {
        var ship=document.querySelector("#Ship");
        return ship;
    }
    this.getMap=function () {
        var map=document.querySelector("#globe");
        return map;
    }

    this.fillShippingInfoForm=function (currentOrderId) {
        var info=viewContext.getInfo();
        var ship=viewContext.getShip();
        var map=viewContext.getMap();
        info.style.display="none";
        map.style.display="none";
        ship.style.display="block";
        var addressMeanings=document.querySelector("#Address_meanings");
        addressMeanings.innerHTML='';
        for (var key in Orders[currentOrderId].shipTo){
            //console.log(key);
            addressMeanings.innerHTML+=Orders[currentOrderId].shipTo[key]+"<br>";
        }
    }
    this.fillCustomerInfoForm=function (currentOrderId) {
        var info=viewContext.getInfo();
        var ship=viewContext.getShip();
        var map=viewContext.getMap();

        info.style.display="block";
        ship.style.display="none";
        map.style.display="none";
        var addressMeanings=document.querySelector("#Customer_meanings");
        addressMeanings.innerHTML='';
        for (var key in Orders[currentOrderId].customerInfo){
            //console.log(key);
            addressMeanings.innerHTML+=Orders[currentOrderId].customerInfo[key]+"<br>";
        }
        var addressMargins=document.querySelector("#Customer_margins");
        addressMargins.innerHTML='';
        for (var key in Orders[currentOrderId].customerInfo){
            addressMargins.innerHTML+=key[0].toUpperCase() + key.slice(1)+":"+"<br>";
        }
    }

    this.getCurrentOrderId = function () {

        var orderIdInput = document.querySelector("#OrderIdInput");
        return orderIdInput.value;
    };

    this.getOrderSearchButton = function () {
        return document.querySelector("#OrderSearchButton");
    };

    this.getDialogButton=function () {

            return document.querySelector("#dialog");
    }



    this.createList=function(value) {
        let ul = this.getListForm();
        let li = document.createElement("li");
        let order_num = document.createElement("div");
        let order_date = document.createElement("div");
        let num = document.createElement("h4");
        let customer = document.createElement("h5");
        let shipped = document.createElement("h5");
        let ordered = document.createElement("h3");
        let status = document.createElement("h5");
        li.id = value;
        num.innerText = "Order "+value;
        customer.innerText = Orders[value]["summary"].customer;
        shipped.innerText = "Shipped:" + Orders[value]["summary"].shippedAt;
        ordered.innerText = Orders[value]["summary"].createdAt;
        status.innerText = Orders[value]["summary"].status;
        order_num.className = "Order__num";
        order_date.className = "Order__date";
        ul.appendChild(li);
        li.appendChild(order_num);
        li.appendChild(order_date);
        order_num.appendChild(num);
        order_num.appendChild(customer);
        order_num.appendChild(shipped);
        order_date.appendChild(ordered);
        order_date.appendChild(status);

        li.addEventListener("click", this.listElementClick);}

/////////////////////////////////////////////////////////////
    this.listElementClick=function(){
    _currentOrder=this.id;
    var ordersInform=document.querySelector("#Order_Inform");
    ordersInform.innerHTML="";
    for (var key in Orders[_currentOrder]["summary"]) {
        ordersInform.innerHTML+=key[0].toUpperCase() + key.slice(1)+": "+Orders[_currentOrder]["summary"][key]+ "<br>";
    }
    if(shippingButtonOn){
        viewContext.fillShippingInfoForm(_currentOrder);
    }
    else {
        viewContext.fillCustomerInfoForm(_currentOrder);
    }
    };              /////------>transform to MVC PATTERN
    this.Order_information=function() {
        var Order_inform=document.querySelector("#Order_Inform");
        Order_inform.innerText="";
        var Order_num=document.createElement("h3");
        var Customer=document.createElement("h5");
        var Ordered=document.createElement("h5");
        var Shipped=document.createElement("h5");
        Order_num.innerText="Order "+id;
        Customer.innerText="Customer "+Orders[id].summary.customer;
        Ordered.innerText="Ordered "+Orders[id].summary.createdAt;
        Shipped.innerText="Shipped "+Orders[id].summary.shippedAt;
        Order_inform.appendChild(Order_num);
        Order_inform.appendChild(Customer);
        Order_inform.appendChild(Ordered);
        Order_inform.appendChild(Shipped);
    }             /////
    this.viewAllOrders=function(Orders) {
        var ul=document.querySelector("#ul");
        ul.innerHTML="";
        for (var i=0;i<Orders.length;i++){
            viewContext.createList(i);
        }

    }           /////
    /////////////////////////////////////////////////////////



    this.loadGoogleAddress = function (address) {
        var info=viewContext.getInfo();
        var ship=viewContext.getShip();
        var map=viewContext.getMap();
        info.style.display="none";
        ship.style.display="none";
        map.style.display="block";

        var geocoder, map;
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address': address
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var myOptions = {
                    zoom: 16,
                    center: results[0].geometry.location,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    fullscreenControl:true
                };
                map = new google.maps.Map(document.querySelector(".globe"), myOptions);

                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            }
        });
        return this;
    };
}

function Controller(view, model) {
    this.init = function() {
        model.getURL("http://localhost:3000/api/Orders",view.viewAllOrders);
        var searchInputField = view.getSearchInputField();
        var searchInputButton = view.getSearchInputButton();
        var shippingAddressButton=view.getShippingAddressButton();
        var customerInfoButton=view.getCustomerInfoButton();
        var DialogButton=view.getDialogButton();
        var closeOrderModelButton = view.getCloseOrderModelButton();
        var closeProductModelButton = view.getCloseProductModelButton();
        var closeUpdateModelButton = view.getCloseUpdateModelButton();
        var updateInfoButton = view.getUpdateInfoButton();
        var addProductButton = view.getAddProductButton();
        var mapButton=view.getMapButton();
        var subInfoButton = view.getSubInfoButton();
        var subOrderButton = view.getSubOrderButton();


        shippingAddressButton.addEventListener("click",this._onShippingButtonClick);
        customerInfoButton.addEventListener("click",this._onCustomerInfoButtonClick);
        searchInputField.addEventListener("keyup",this._onSearchInputField);
        DialogButton.addEventListener("click",this._onDialogButtonClick);
        closeOrderModelButton.addEventListener("click", this.onCloseOrderModelButton);
        closeProductModelButton.addEventListener("click", this.onCloseProductModelButton);
        closeUpdateModelButton.addEventListener("click", this.onCloseUpdateModelButton);
        updateInfoButton.addEventListener("click", this.onUpdateInfoButtonClick);
        addProductButton.addEventListener("click", this.onAddProductButtonClick);
        mapButton.addEventListener("click", this.onMapButtonClick);
        subInfoButton.addEventListener("click", this.onSubInfoButtonClick);
        subOrderButton.addEventListener("click", this.onSubOrderButtonClick);


    };


    this._onDialogButtonClick=function (e) {
        var inputs = view.getOrderModelInputs();
        view.clearInputs(inputs);
        view.openModel("orderModel");
    }
///////////////////////////////////////////////////////////////////
    this.onUpdateInfoButtonClick = function () {
        var inputs = view.getUpdateModelInputs();
        view.clearInputs(inputs);
        view.setCurrentValue(inputs, Orders[_currentOrder]);
        view.openModel("updateModel");
    };
    this.onMapButtonClick=function () {
        view.loadGoogleAddress(Orders[_currentOrder].shipTo.country + "," + Orders[_currentOrder].shipTo.region + "," + Orders[_currentOrder].shipTo.address);

    }


    this.onSubOrderButtonClick = function () {
        var summaryCustomer = view.getSummaryCustomerValue();
        var summaryShippedAt = view.getSummaryShippedAtValue();
        var summaryStatus = view.getSummaryStatusValue();
        var summaryTotalPrice = view.getSummaryTotalPriceValue();

        var shipToName = view.getShipToNameValue();
        var shipToStreet = view.getShipToStreetValue();
        var shipToZipCode = view.getShipToZipCodeValue();
        var shipToRegion = view.getShipToRegionValue();
        var shipToCountry = view.getShipToCountryValue();

        var customerInfoFirstName = view.getCustomerInfoFirstNameValue();
        var customerInfoLastName = view.getCustomerInfoLastNameValue();
        var customerInfoAddress = view.getCustomerInfoAddressValue();
        var customerInfoPhone = view.getCustomerInfoPhoneValue();
        var customerInfoEmail = view.getCustomerInfoEmailValue();
        var date = new Date();
        var newOrder = {
            "summary": {
                "createdAt": date.getDate() + "." + date.getMonth() + "." + date.getFullYear(),
                "customer": summaryCustomer,
                "status": summaryStatus,
                "shippedAt": summaryShippedAt
            },
            "shipTo": {
                "name": shipToName,
                "address": shipToStreet,
                "ZIP": shipToZipCode,
                "region": shipToRegion,
                "country": shipToCountry
            },
            "customerInfo": {
                "firstName": customerInfoFirstName,
                "lastName": customerInfoLastName,
                "address": customerInfoAddress,
                "phone": customerInfoPhone,
                "email": customerInfoEmail
            }
        };

        model.createOrder(newOrder)
            .then(function (data) {
                Orders = data;

                view.closeModel("orderModel");
            })
            .catch(function (error) {
                console.log(error.name + error)
            });
        view.viewAllOrders(Orders);
    };

    this.onAddProductButtonClick = function () {
        var inputs = view.getProductModelInputs();
        view.clearInputs(inputs);
        view.openModel("productModel");
    };

    this.onSubInfoButtonClick = function () {
        var updateName = view.getUpdateNameValue();
        var updateStreet = view.getUpdateStreetValue();
        var updateZipCode = view.getUpdateZipCodeValue();
        var updateRegion = view.getUpdateRegionValue();
        var updateCountry = view.getUpdateCountryValue();
        var shipTo = ShipTo;
        shipTo.name = updateName;
        shipTo.address = updateStreet;
        shipTo.ZIP = updateZipCode;
        shipTo.region = updateRegion;
        shipTo.country = updateCountry;

        model.sendRequest("POST", '/orders/update?where={"id":' + +(_currentOrder+1) + '}', shipTo)
            .then(function (data) {
                console.log("Comes from backend"+data);

                model.getURL("http://localhost:3000/api/Orders",view.viewAllOrders);
                view.closeModel("updateModel");
            })
            .catch(function (error) {
                console.log(error)
            });
    };
/////////////////////////////////////////////////////////////////////

    this.getUpdateModelInputs = function () {
        return document.querySelectorAll("#updateModel input");
    };

    this._onShippingButtonClick=function (e) {
        view.fillShippingInfoForm(_currentOrder);
        shippingButtonOn=true;
    }

    this._onCustomerInfoButtonClick=function (e) {
        view.fillCustomerInfoForm(_currentOrder);
        shippingButtonOn=false;
    }

    this._onSearchInputField=function (e) {
        var ul=document.querySelector("#ul");
        ul.innerHTML="";
        var value=view.getCurrentSearchValue();
        if(value=="")
        {
        view.viewAllOrders(Orders)
        }else {
            console.log(value);
            view.createList(model.find(value));
        }
    }

    this._onSearchOrderClick = function(e) {
        var orderId = view.getCurrentOrderId();

        model
            .fetchOrderById(orderId)
            .then(function (orderData) {
                view.fillOrderForm(orderData);
            });
    };

    this._onLoadEmployeeInfoClick = function(e) {
        model
            .fetchEmployee()
            .then(function (employeeDate) {
                view.fillEmployeeForm(employeeDate);
            });
    };

    this._onLoadTerritoriesClick = function (e) {
        model
            .fetchTerritories()
            .then(function (territoriesData) {
                view.fillTerritoriesList(territoriesData);
            });
    }

    this.onCloseOrderModelButton = function () {
        view.closeModel("orderModel");
    };

    this.onCloseProductModelButton = function () {
        view.closeModel("productModel");
    };

    this.onCloseUpdateModelButton = function () {
        view.closeModel("updateModel");
    };
}

(new Controller(new View, new Model)).init();